package com.sapient.spring.basic.config;

import com.sapient.spring.basic.pojo.Circle;
import com.sapient.spring.basic.pojo.Shape;
import com.sapient.spring.basic.pojo.Triangle;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.List;

@Configuration
public class AppConfig {

    //Singleton-scoped Bean
    @Bean("circle")
    public Shape getCircleBean()
    {
        Shape circle = new Circle();
        return circle;
    }

    /*
    Method to create Bean of type List<Shape>.
    We can create beans for:
    - List
    - Set
    - Map
    - Properties
     */
    @Bean
    public List<Shape> getMultiShapes()
    {
        List<Shape> shapes = new ArrayList<Shape>();
        shapes.add(new Circle());
        shapes.add(new Triangle());

        return shapes;
    }

}
