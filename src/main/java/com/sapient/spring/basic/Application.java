package com.sapient.spring.basic;

import com.sapient.spring.basic.draw.DrawingApplication;
import com.sapient.spring.basic.draw.MultiShapesApp;
import com.sapient.spring.basic.pojo.Triangle;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.sapient.spring.basic.*")
public class Application {

    public static void main(String... args) {

        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
        context.register(Application.class);
        context.refresh();

        System.out.println("Drawing Application Started");

        //To check single scenario
        System.out.println("Checking single scenario----------");
        DrawingApplication dr = context.getBean(DrawingApplication.class);
        dr.drawShape();
        System.out.println("Shape Area:"+dr.getShapeArea());


        //To check prototype-scoped bean scenario
        System.out.println("Checking prototype-scoped bean scenario----------");
        Triangle triangle = context.getBean(Triangle.class);
        System.out.println("Triangle Area:"+triangle.getArea());

        triangle = context.getBean(Triangle.class);
        System.out.println("Triangle Area for instance 2:"+triangle.getArea());

        //To check multi-shapes scenario
        System.out.println("Checking bean of type list scenario--------------");
        MultiShapesApp app = context.getBean(MultiShapesApp.class);
        app.drawShapes();
        app.printShapeArea();

        context.close();

        System.out.println("Drawing Application Closed");
    }
}
