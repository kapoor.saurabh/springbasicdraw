package com.sapient.spring.basic.draw;

import com.sapient.spring.basic.pojo.Shape;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class MultiShapesApp {

    @Autowired
    private List<Shape> shapes;

    public void drawShapes() {

        System.out.println("Drawing All shapes..");
        for(Shape shape:shapes) {
            shape.draw();
        }
    }

    public void printShapeArea() {

        System.out.println("Printing All Areas...");
        for(Shape shape:shapes) {
            System.out.println(shape.getArea());
        }
    }

}
