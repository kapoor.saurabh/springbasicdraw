package com.sapient.spring.basic.draw;

import com.sapient.spring.basic.pojo.Shape;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class DrawingApplication {

    @Autowired
    @Qualifier("circle")
    private Shape shape;

    public void drawShape() {
        shape.draw();
    }

    public double getShapeArea() {
        return shape.getArea();
    }
}
