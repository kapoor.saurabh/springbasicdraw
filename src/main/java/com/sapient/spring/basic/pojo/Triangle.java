package com.sapient.spring.basic.pojo;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component("triangle")
@Scope("prototype")
public class Triangle implements Shape {

    private double area;

    @PostConstruct
    void init() {
        this.area = Math.random() * 100;
    }

    public void draw() {
        System.out.println("Draw Triangle");
    }

    public double getArea() {
        return area;
    }
}
