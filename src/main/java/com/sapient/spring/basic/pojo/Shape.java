package com.sapient.spring.basic.pojo;

public interface Shape {

    //Method signature to draw shape
    void draw();

    //Method signature to check shape area
    double getArea();

}
